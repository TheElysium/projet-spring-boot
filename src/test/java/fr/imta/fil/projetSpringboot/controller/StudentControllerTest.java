package fr.imta.fil.projetSpringboot.controller;

import fr.imta.fil.projetSpringboot.RequestTestUtils;
import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.CourseWithStudents;
import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.model.StudentCourse;
import fr.imta.fil.projetSpringboot.repository.CourseRepository;
import fr.imta.fil.projetSpringboot.repository.FollowRepository;
import fr.imta.fil.projetSpringboot.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

@Transactional
public class StudentControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private FollowRepository followRepository;

    private static final String URL = "http://localhost:8080/api/student";

    @Test
    public void getStudentsTest(){

        ArrayList<Student> allStudents = new ArrayList<>();

        studentRepository.findAll().forEach((Student s) -> {
            allStudents.add(s);
        });

        RequestEntity<Void> request = RequestEntity.get(URL+"/").build();
        ResponseEntity<Student[]> response = testRestTemplate.exchange(request, Student[].class);

        RequestTestUtils.checkValidResponse(response);

        Student[] responseBody = response.getBody();

        assertThat(responseBody.length).isEqualTo(allStudents.size());

        for (int i = 0; i < allStudents.size(); i++) {
            Student studentInDb = allStudents.get(i);
            Student studentInResponse = responseBody[i];
            assertThat(studentInDb.getFirstname()).isEqualTo(studentInResponse.getFirstname());
            assertThat(studentInDb.getLastname()).isEqualTo(studentInResponse.getLastname());
            assertThat(studentInDb.getId()).isEqualTo(studentInResponse.getId());
        }
    }


    @Test
    public void getStudentsNoteInferieur(){
        RequestEntity<Void> request = RequestEntity.get(URL+"/LowGrade/7").build();
        ResponseEntity<Student[]> response = testRestTemplate.exchange(request, Student[].class);

        RequestTestUtils.checkValidResponse(response);
        assertThat(response.getBody()[0].getId()).isEqualTo(2);
    }

    @Test
    public void getStudent(){
        Student s = studentRepository.findById(1).get();

        RequestEntity<Void> request = RequestEntity.get(URL+"/withName?lastname=Barthelme&firstname=Justine").build();
        ResponseEntity<StudentCourse> response = testRestTemplate.exchange(request, StudentCourse.class);

        RequestTestUtils.checkValidResponse(response);
        assertThat(s.getId()).isEqualTo(response.getBody().getStudent().getId());
    }
}

