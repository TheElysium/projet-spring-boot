package fr.imta.fil.projetSpringboot.controller;

import fr.imta.fil.projetSpringboot.RequestTestUtils;
import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.CourseWithStudents;
import fr.imta.fil.projetSpringboot.repository.CourseRepository;
import fr.imta.fil.projetSpringboot.repository.FollowRepository;
import fr.imta.fil.projetSpringboot.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

@Transactional
public class CourseControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private FollowRepository followRepository;

    private static final String URL = "http://localhost:8080/api/course";

    @Test
    public void getCoursesTest(){

        ArrayList<Course> allCourses = new ArrayList<>();

        courseRepository.findAll().forEach((Course c) -> {
            allCourses.add(c);
        });

        RequestEntity<Void> request = RequestEntity.get(URL+"/").build();
        ResponseEntity<Course[]> response = testRestTemplate.exchange(request, Course[].class);

        RequestTestUtils.checkValidResponse(response);

        Course[] responseBody = response.getBody();

        assertThat(responseBody.length).isEqualTo(allCourses.size());

        for (int i = 0; i < allCourses.size(); i++) {
            Course courseInDb = allCourses.get(i);
            Course courseInResponse = responseBody[i];
            assertThat(courseInDb.getTitle()).isEqualTo(courseInResponse.getTitle());
            assertThat(courseInDb.getDescription()).isEqualTo(courseInResponse.getDescription());
            assertThat(courseInDb.getId()).isEqualTo(courseInResponse.getId());
        }
    }

    // La transaction ne se rollback pas !!!
    @Test
    public void createCourseTest(){
        Course c = new Course("Test de cours", "Oui c'est un test");
        RequestEntity<?> request = RequestTestUtils.postRequest(URL +"/", c);

        ResponseEntity<Course> response = testRestTemplate.exchange(request, Course.class);

        RequestTestUtils.checkValidResponse(response);

        System.out.println(courseRepository.getCourseByTitle("Test de cours").isPresent());
        assertThat(courseRepository.getCourseByTitle("Test de cours").isPresent()).isTrue();

    }

    // La transaction ne se rollback pas !!!
    @Test
    public void createCourseWithStudents(){
        CourseWithStudents courseWithStudents = new CourseWithStudents(new Course("Autre test de cours", "Oui c'est un test"), new ArrayList<>(Arrays.asList(1,2)));

        RequestEntity<?> request = RequestTestUtils.postRequest(URL+"/createCourseWithStudents", courseWithStudents);

        ResponseEntity<CourseWithStudents> response = testRestTemplate.exchange(request, CourseWithStudents.class);

        RequestTestUtils.checkValidResponse(response);

        assertThat(courseRepository.getCourseByTitle("Autre test de cours").isPresent()).isTrue();

        Course c = courseRepository.getCourseByTitle("Autre test de cours").get();

        assertThat(followRepository.getFollowsByStudentId(1).stream().filter(
                follow -> follow.getCourse_id() == c.getId()
        ).findAny().isPresent()).isTrue();

        assertThat(followRepository.getFollowsByStudentId(2).stream().filter(
                follow -> follow.getCourse_id() == c.getId()
        ).findAny().isPresent()).isTrue();
    }


}

