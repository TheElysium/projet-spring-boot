package fr.imta.fil.projetSpringboot;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

public class RequestTestUtils {
    public static void checkValidResponse(ResponseEntity response){
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
    }

    public static RequestEntity<?> postRequest(String url, Object o){
        return RequestEntity.post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .body(o);
    }
}
