package fr.imta.fil.projetSpringboot.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class CourseTest {

    @Test
    public void test(){
        Course c = new Course("IHM" , "oui ihm");
        c.setId(1234);
        c.setTitle("C logicielle");
        c.setDescription("oui logiciel");
        assertThat(c.getTitle()).isEqualTo("C logicielle");
        assertThat(c.getDescription()).isEqualTo("oui logiciel");
        assertThat(c.getId()).isEqualTo(1234);
    }

}
