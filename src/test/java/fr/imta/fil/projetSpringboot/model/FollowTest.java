package fr.imta.fil.projetSpringboot.model;

import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.repository.CourseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class FollowTest {

    @Test
    public void test(){
        Follow c = new Follow(1 , 1, 10);
        c.setStudent_id(2);
        c.setCourse_id(2);
        c.setEvaluation(3);
        assertThat(c.getCourse_id()).isEqualTo(2);
        assertThat(c.getStudent_id()).isEqualTo(2);
        assertThat(c.getEvaluation()).isEqualTo(3);
    }

}