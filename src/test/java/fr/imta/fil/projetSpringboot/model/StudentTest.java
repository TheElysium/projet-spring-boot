package fr.imta.fil.projetSpringboot.model;

import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.repository.CourseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class StudentTest {

    @Test
    public void test(){
        Student s = new Student("Donovan" , "Brun");
        s.setId(1234);
        s.setFirstname("Justine");
        s.setLastname("Barthelme");
        assertThat(s.getFirstname()).isEqualTo("Justine");
        assertThat(s.getLastname()).isEqualTo("Barthelme");
        assertThat(s.getId()).isEqualTo(1234);
    }

}
