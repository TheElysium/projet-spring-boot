package fr.imta.fil.projetSpringboot.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@Transactional
public class CourseWithStudentsTest {
    @Test
    public void test(){
        CourseWithStudents courseWithStudents = new CourseWithStudents(new Course("test", "test"), new ArrayList<Integer>(Arrays.asList(1,2)));
        Course c = new Course("modif", "modif");
        courseWithStudents.setCourse(c);
        ArrayList<Integer> l = new ArrayList<Integer>(Arrays.asList(3,4));
        courseWithStudents.setStudentList(l);

        assertThat(courseWithStudents.getCourse()).isEqualTo(c);
        assertThat(courseWithStudents.getStudentList()).isEqualTo(l);
    }
}
