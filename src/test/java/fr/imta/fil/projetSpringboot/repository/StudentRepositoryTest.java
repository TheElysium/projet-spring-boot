package fr.imta.fil.projetSpringboot.repository;

import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class StudentRepositoryTest {

    @Autowired
    StudentRepository studentRepository;

    @Test
    public void contextLoads(){
        assertThat(studentRepository).isNotNull();
    }

    @Test
    public void addStudent(){
        Student s = new Student("Donovan", "Brun");
        s = studentRepository.save(s);
        assertThat(studentRepository.findById(s.getId()).isPresent()).isTrue();
        assertThat(s.getId()).isEqualTo(studentRepository.findById(s.getId()).get().getId());
    }

    @Test
    public void modifyStudent(){
        Student s = new Student("Donovan", "Brun");
        s = studentRepository.save(s);

        Student sModified = studentRepository.findById(s.getId()).get();
        sModified.setFirstname("Justine");
        sModified.setLastname("Barthelme");
        sModified = studentRepository.save(s);

        assertThat(s.getFirstname()).isEqualTo(studentRepository.findById(s.getId()).get().getFirstname());
        assertThat(s.getLastname()).isEqualTo(studentRepository.findById(s.getId()).get().getLastname());
    }

    @Test
    public void deleteStudent(){
        Student s = new Student("Donovan", "Brun");
        s = studentRepository.save(s);
        studentRepository.delete(s);

        assertThat(studentRepository.findById(s.getId()).isPresent()).isFalse();
    }
}
