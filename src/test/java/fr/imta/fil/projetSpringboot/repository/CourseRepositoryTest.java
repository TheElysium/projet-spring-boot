package fr.imta.fil.projetSpringboot.repository;

import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.repository.CourseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
public class CourseRepositoryTest {

    @Autowired
    CourseRepository courseRepository;

    @Test
    public void contextLoads(){
        assertThat(courseRepository).isNotNull();
    }

    @Test
    public void addCourse(){
        Course c = new Course("Test", "J'adore Spring (non)");
        c = courseRepository.save(c);
        assertThat(courseRepository.findById(c.getId()).isPresent()).isTrue();
        assertThat(c.getId()).isEqualTo(courseRepository.findById(c.getId()).get().getId());
    }

    @Test
    public void modifyCourse(){
        Course c = new Course("Conception logicielle", "J'adore Spring (non)");
        c = courseRepository.save(c);

        Course cModified = courseRepository.findById(c.getId()).get();
        cModified.setTitle("IHM");
        cModified.setTitle("Oui les interfaces");
        cModified = courseRepository.save(c);

        assertThat(c.getTitle()).isEqualTo(courseRepository.findById(c.getId()).get().getTitle());
        assertThat(c.getDescription()).isEqualTo(courseRepository.findById(c.getId()).get().getDescription());
    }

    @Test
    public void deleteCourse(){
        Course c = new Course("Conception logicielle", "J'adore Spring (non)");
        c = courseRepository.save(c);
        courseRepository.delete(c);

        assertThat(courseRepository.findById(c.getId()).isPresent()).isFalse();
    }
}
