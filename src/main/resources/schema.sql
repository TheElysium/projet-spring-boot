drop table if exists `Follow`;
drop table if exists `Course`;
drop table if exists `Student`;


CREATE TABLE `Student` (
                           `id` int PRIMARY KEY auto_increment,
                           `firstname` varchar(40),
                           `lastname` varchar(40)
);

CREATE TABLE `Course` (
                          `id` int PRIMARY KEY auto_increment,
                          `title` varchar(40),
                          `description` varchar(40)
);

CREATE TABLE `Follow` (
                          `id` int PRIMARY KEY auto_increment,
                          `student_id` int,
                          `course_id` int,
                          `evaluation` int
);

ALTER TABLE `Follow` ADD FOREIGN KEY (`student_id`) REFERENCES `Student` (`id`);

ALTER TABLE `Follow` ADD FOREIGN KEY (`course_id`) REFERENCES `Course` (`id`);
