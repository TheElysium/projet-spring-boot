DELETE FROM Follow;
DELETE FROM Student;
DELETE FROM Course;

INSERT INTO Student Values (1, "Justine", "Barthelme");
INSERT INTO Student Values (2, "Luka", "Signe--Morice");
INSERT INTO Student Values (3, "Donovan", "Brun");

INSERT INTO Course Values (1, "Mathématiques de bases", "Algèbre linéaire et analyse");
INSERT INTO Course Values (2, "Conception logicielle", "Spring");
INSERT INTO Course Values (3, "IHM", "Développement d'un tableau de bord");


INSERT INTO Follow Values (1, 1, 1, 10);
INSERT INTO Follow Values (2, 2, 1, 15);
INSERT INTO Follow Values (3, 3, 1, 8);

INSERT INTO Follow Values (4, 1, 2, 12);
INSERT INTO Follow Values (5, 2, 2, 7);

INSERT INTO Follow Values (6, 3, 3, 13);