package fr.imta.fil.projetSpringboot.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("Follow")
public class Follow{

    @Id
    private int id;
    private int student_id;
    private int course_id;
    private int evaluation;


    public Follow(int student_id, int course_id, int evaluation) {
        this.student_id = student_id;
        this.course_id = course_id;
        this.evaluation = evaluation;
    }

    public Integer getId() {
        return id;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }
}
