package fr.imta.fil.projetSpringboot.model;

import java.util.ArrayList;

public class StudentCourse {

    private Student student;
    private ArrayList<Course> courses;

    public StudentCourse(Student student, ArrayList<Course> courses) {
        this.student = student;
        this.courses = courses;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }
}
