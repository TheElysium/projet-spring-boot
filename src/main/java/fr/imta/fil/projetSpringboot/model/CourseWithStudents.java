package fr.imta.fil.projetSpringboot.model;

import java.util.List;

/**
 * Le JSON de la requête Post createCourseWithStudents dans CourseController.class
 * sera mappée à cette classe.
 */
public class CourseWithStudents {

    private Course course;
    private List<Integer> studentList;

    public CourseWithStudents(Course course, List<Integer> studentList) {
        this.course = course;
        this.studentList = studentList;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<Integer> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Integer> studentList) {
        this.studentList = studentList;
    }
}
