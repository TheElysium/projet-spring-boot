package fr.imta.fil.projetSpringboot.repository;

import fr.imta.fil.projetSpringboot.model.Course;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CourseRepository extends CrudRepository<Course, Integer> {
    @Query("SELECT * FROM Course WHERE title = :title")
    Optional<Course> getCourseByTitle(@Param("title") String title);
}
