package fr.imta.fil.projetSpringboot.repository;

import fr.imta.fil.projetSpringboot.model.Student;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {
    
    @Query("SELECT * FROM Student WHERE lastname = :lastname AND firstname = :firstname;")
    Student getStudent(@Param("lastname") String lastname, @Param("firstname") String firstname);

    @Query("select * from Student where id= :idStudent")
    Student getStudentByNote(@Param("idStudent") Integer idStudent);
}
