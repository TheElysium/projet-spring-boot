package fr.imta.fil.projetSpringboot.repository;

import fr.imta.fil.projetSpringboot.model.Follow;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FollowRepository extends CrudRepository<Follow, Integer> {
    
    @Query("select student_id from Follow where evaluation <= :note")
    List<Integer> getStudentIdByNote(@Param("note") int note);

    @Query("SELECT * FROM Follow WHERE student_id = :studentId;")
    List<Follow> getFollowsByStudentId(@Param("studentId") int studentId);
}
