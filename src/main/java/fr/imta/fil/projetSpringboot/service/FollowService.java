package fr.imta.fil.projetSpringboot.service;

import fr.imta.fil.projetSpringboot.repository.FollowRepository;
import fr.imta.fil.projetSpringboot.model.Follow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FollowService {

    private FollowRepository followRepository;

    @Autowired
    public FollowService(FollowRepository followService) {
        this.followRepository=followService;
    }

    public List<Integer> getStudentByNote(int note){
        return followRepository.getStudentIdByNote(note);
    }

    public List<Follow> getFollows() {

        ArrayList<Follow> follows = new ArrayList<>();

        followRepository.findAll().forEach((Follow follow) -> {
            follows.add(follow);
        });

        return follows;
    }

    public List<Follow> getFollowsByStudentId(int studentId) {

        return followRepository.getFollowsByStudentId(studentId);
    }

    public void add(Follow follow){
        followRepository.save(follow);
    }
}
