package fr.imta.fil.projetSpringboot.service;

import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.Follow;
import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.model.StudentCourse;
import fr.imta.fil.projetSpringboot.repository.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private StudentRepository studentRepository;
    private FollowService followService;
    private CourseService courseService;

    @Autowired
    public StudentService(StudentRepository studentRepository, FollowService followService, CourseService courseService) {
        this.studentRepository = studentRepository;
        this.followService = followService;
        this.courseService = courseService;
    }

    public List<Student> getStudents() {

        ArrayList<Student> students = new ArrayList<>();

        studentRepository.findAll().forEach((Student student) -> {
            students.add(student);
        });

        return students;
    }

    public List<Student> getStudentsNoteInferieur(int note){

        List<Integer> a = followService.getStudentByNote(note);
        List<Student> students = new ArrayList<>();
        a.forEach((Integer i) -> {
            Student student = studentRepository.getStudentByNote(i);

            boolean find = false;
            for (Student s : students) {
                if (s.getId() == student.getId()) {
                    find = true;
                    break;
                }
            }

            if (!find) students.add(student);
        });

        return students;
    }

    public StudentCourse getStudentWithCourse(String lastname, String firstname) {

        // Get the student with his lastname and firstname
        Student student = this.studentRepository.getStudent(lastname, firstname);

        ArrayList<Course> coursesFollowedByStudent = new ArrayList<>();

        if (student != null) {
            // Get all courses followed by the student
            ArrayList<Follow> follows = (ArrayList<Follow>) followService.getFollowsByStudentId(student.getId());
            follows.forEach((follow) -> {
                // For all follow, verify if the course is present and add in the followed courses list
                Optional<Course> optCourse = courseService.getCourseById(follow.getCourse_id());
                if (optCourse.isPresent()) {
                    coursesFollowedByStudent.add(optCourse.get());
                }
            });
        }

        // Return a Object with the student and the list of followed courses
        return new StudentCourse(student, coursesFollowedByStudent);
    }
}
