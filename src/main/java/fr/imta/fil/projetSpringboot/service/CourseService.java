package fr.imta.fil.projetSpringboot.service;

import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.CourseWithStudents;
import fr.imta.fil.projetSpringboot.model.Follow;
import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.repository.CourseRepository;
import fr.imta.fil.projetSpringboot.repository.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    private CourseRepository courseRepository;
    private FollowService followService;
    private StudentRepository studentRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository, FollowService followService, StudentRepository studentRepository) {
        this.courseRepository = courseRepository;
        this.followService = followService;
        this.studentRepository = studentRepository;
    }

    public List<Course> getCourses() {

        ArrayList<Course> courses = new ArrayList<>();

        courseRepository.findAll().forEach((Course course) -> {
            courses.add(course);
        });

        return courses;
    }

    public Course add(Course course){
        courseRepository.save(course);
        return course;
    }

    /**
     * Créé un cours et lui associe les étudiants qui
     * y assistent
     *
     * @param courseWithStudents
     * @return CourseWithStudents
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public CourseWithStudents createCourseWithStudents(CourseWithStudents courseWithStudents) throws Exception {

        Optional<Course> courseInTable = courseRepository.getCourseByTitle(courseWithStudents.getCourse().getTitle());

        //Si le cours n'est déjà présent dans la base de données, on lève une exception
        if(courseInTable.isPresent()) {
            throw new Exception("Le cours identifié " + courseInTable.get().getId().toString() + " est déjà présent dans la table !") ;
        }

        //Ajout du nouveau cours dans la base de données
        Course course = courseWithStudents.getCourse();
        course = add(course);

        for (Integer studentId: courseWithStudents.getStudentList()) {
            Optional<Student> student = studentRepository.findById(studentId);
            if(student.isPresent()){
                Follow f = new Follow(student.get().getId(), course.getId(),0);
                followService.add(f);
            }
            else{
                throw new Exception("L'élève identifié " + studentId.toString() + "n'est pas présent dans la base de données !") ;
            }

        }

        return courseWithStudents;
    }
    
    public Optional<Course> getCourseById(int id) {
        return courseRepository.findById(id);
    }
}
