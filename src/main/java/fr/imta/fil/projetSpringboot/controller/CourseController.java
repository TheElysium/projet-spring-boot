package fr.imta.fil.projetSpringboot.controller;

import fr.imta.fil.projetSpringboot.model.Course;
import fr.imta.fil.projetSpringboot.model.CourseWithStudents;
import fr.imta.fil.projetSpringboot.service.CourseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="api/course")
public class CourseController {

    private CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public List<Course> getCourses() {
        return courseService.getCourses();
    }

    @PostMapping
    public Course createCourse(@RequestBody Course course){
        return courseService.add(course);
    }

    @PostMapping("/createCourseWithStudents")
    public CourseWithStudents createCourseWithStudents(@RequestBody CourseWithStudents courseWithStudents) throws Exception {
        return courseService.createCourseWithStudents(courseWithStudents);
    }
}
