package fr.imta.fil.projetSpringboot.controller;

import fr.imta.fil.projetSpringboot.model.Student;
import fr.imta.fil.projetSpringboot.model.StudentCourse;
import fr.imta.fil.projetSpringboot.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RestController
@RequestMapping(path="api/student")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> getStudents() {return studentService.getStudents();}

    @GetMapping("LowGrade/{note}")
    public List<Student> getStudentsNoteInferieur(@PathVariable int note) {return studentService.getStudentsNoteInferieur(note);}


    @GetMapping("/withName") // ?lastname=...&firstname=...
    public ResponseEntity<StudentCourse> getStudent(@RequestParam String lastname, @RequestParam String firstname) {

        StudentCourse studentCourse = studentService.getStudentWithCourse(lastname, firstname);
        if (studentCourse.getStudent() != null) {
            return new ResponseEntity<StudentCourse>(studentCourse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
